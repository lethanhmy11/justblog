﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.ViewModels.Comments
{
    public class CommentViewModel
    {
        public int Id { set; get; }

        public string Name { set; get; }

        public string CommentHeader { set; get; }

        public DateTime ModifiedOn { get; set; }
    }
}
