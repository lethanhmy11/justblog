﻿using Microsoft.AspNetCore.Mvc;
using FA.JustBlog.Services.Categories;
using FA.JustBlog.ViewModels.Categories;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace FA.JustBlog.Web.Areas.Admin.Controllers
{
    [Authorize]
    public class CategoriesController : BaseController
    {
        private readonly ICategoryServices categoryServices;
        private readonly IMapper mapper;

        public CategoriesController(ICategoryServices categoryServices,IMapper mapper)
        {
            this.categoryServices = categoryServices;
            this.mapper = mapper;
        }

        
        public IActionResult Index()
        {
            return View(categoryServices.GetAll());
        }


        public IActionResult Details(int id)
        { 
            var category = categoryServices.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }

        [Authorize(Roles="Contributor, Blog Owen")]
        [HttpGet]
        public IActionResult Create()
        {           
            CreateCategoryViewModel category = new CreateCategoryViewModel();
            return View(category);
        }


        [HttpPost]
        public IActionResult Create( CreateCategoryViewModel category)
        {
            if (ModelState.IsValid)
            {
                var response=categoryServices.Create(category);
                ViewBag.ErrorMessages = response.ErrrorMessages;
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }


        public IActionResult Edit(int id)
        {
            var category = categoryServices.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(mapper.Map<CreateCategoryViewModel>(category));
        }


        [HttpPost]
        public IActionResult Edit(CreateCategoryViewModel category)
        {
            if (ModelState.IsValid)
            {
                categoryServices.Update(category);
                return RedirectToAction(nameof(Index));
            }
            return View(category);
        }


        public IActionResult Delete(int id)
        {
            var category = categoryServices.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }


        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            categoryServices.Delete(id);
            return RedirectToAction(nameof(Index));
        }

    }
}
