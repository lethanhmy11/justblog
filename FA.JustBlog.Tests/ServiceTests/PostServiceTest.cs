﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Enums;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.Core.IReponsitories;
using FA.JustBlog.Services.Posts;
using FA.JustBlog.ViewModels.Posts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;


namespace FA.JustBlog.Tests.ServiceTests
{
    [TestClass]
    public class PostServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IPostServices _postServices;
        private IMapper _mapper;
        private List<Post> _listPost;

        [SetUp]
        public void Initialize()
        {
            _unitOfWork= new Mock<IUnitOfWork>();
           
            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new Mapper());
                });
                _mapper = mappingConfig.CreateMapper();
            }

            _listPost = new List<Post>()
            {
               new Post { Id = 1, Title = "Post 1", ShortDescription = "New version of the ASP.NET web framework",
               PostContent = "ASP.NET Core is the new version of the ASP.NET web framework ", UrlSlug = "asp-netcore-overview",
               Published = true, CategoryId = 3, ViewCount = 12500, RateCount = 62, TotalRate = 250, Rate = 4.03,
               CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = EntityStatus.Enable },
                new Post { Id = 2, Title = "Post 2", ShortDescription = "New version of the ASP.NET web framework",
               PostContent = "ASP.NET Core is the new version of the ASP.NET web framework ", UrlSlug = "asp-netcore-overview",
               Published = true, CategoryId = 3, ViewCount = 12500, RateCount = 62, TotalRate = 250, Rate = 4.03,
               CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = EntityStatus.Enable },
                new Post { Id = 3, Title = "Post 3", ShortDescription = "New version of the ASP.NET web framework",
               PostContent = "ASP.NET Core is the new version of the ASP.NET web framework ", UrlSlug = "asp-netcore-overview",
               Published = true, CategoryId = 3, ViewCount = 12500, RateCount = 62, TotalRate = 250, Rate = 4.03,
               CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = EntityStatus.Enable }
            };
            _postServices = new PostServices(_unitOfWork.Object, _mapper);
        }

        [Test]
        public void PostSevices_GetAll_ReturnListPost()
        {
            _unitOfWork.Setup(p => p.PostResponsitory.GetAll()).Returns(_listPost);
            var result = _postServices.GetAll();
            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count);
        }

        [Test]
        public void PostSevices_Create_ReturnListPost()
        {
            CreatePostViewModel postViewModel = new CreatePostViewModel()
            {
                Id=4,
                Title ="New Post",
                UrlSlug = "new-post",
                ShortDescription ="Short Desciption",
                PostContent ="Post Content",
                CategoryId =1,
                Published = true,
                Status = EntityStatus.Enable,
                Tags = "Tag 1",
            };
            var list = new[] { 1 };
            _unitOfWork.Setup(p => p.PostResponsitory.GetAll()).Returns(_listPost);
            _unitOfWork.Setup(p => p.TagResponsitory.AddTagByString(postViewModel.Tags)).Returns(list);
            var result=_postServices.Create(postViewModel);
            
           // Assert.AreEqual(result, new ResponseResult<CreatePostViewModel>());
           // Assert.AreEqual(3, result.Count);
        }
    }

    public class Mapper : Profile
    {
        public Mapper()
        {            
            CreateMap<Post, CreatePostViewModel>().ReverseMap();
            CreateMap<Post, PostViewModel>().ReverseMap();
            CreateMap<Post, PostReviewViewModel>().ReverseMap();
        }
    }
}
