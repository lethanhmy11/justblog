using FA.JustBlog.Core;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.Services.Accounts;
using FA.JustBlog.Services.Categories;
using FA.JustBlog.Services.Comments;
using FA.JustBlog.Services.Mappings;
using FA.JustBlog.Services.Posts;
using FA.JustBlog.Services.Roles;
using FA.JustBlog.Services.Tags;
using FA.JustBlog.Services.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using ReflectionIT.Mvc.Paging;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("JustBlogConnection");
builder.Services.AddDbContext<JustBlogContext>(options =>
    options.UseSqlServer(connectionString));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

//builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
//    .AddEntityFrameworkStores<JustBlogContext>();

//builder.Services.AddIdentity<AppUser, IdentityRole>().AddEntityFrameworkStores<JustBlogContext>().AddDefaultTokenProviders();

builder.Services.AddIdentity<AppUser, IdentityRole>(options =>
{
    options.SignIn.RequireConfirmedEmail = false;
    options.Password.RequireDigit = false;
    options.Password.RequireNonAlphanumeric = false;
    options.Password.RequiredLength = 6;
    options.Password.RequireUppercase = false;
}).AddEntityFrameworkStores<JustBlogContext>();

builder.Services.ConfigureApplicationCookie(options =>
{
    options.AccessDeniedPath = "/access-denied";
    options.LoginPath = "/login";
    //options.ExpireTimeSpan = TimeSpan.FromMinutes(1);
    //options.SlidingExpiration = true;
});

builder.Services.AddScoped<IUnitOfWork, UnitOfWork>();
builder.Services.AddScoped<ICategoryServices, CategoryServices>();
builder.Services.AddScoped<IPostServices, PostServices>();
builder.Services.AddScoped<ITagServices, TagServices>();
builder.Services.AddScoped<ICommentServices, CommentServices>();
builder.Services.AddScoped<IUserServices, UserServices>();
builder.Services.AddScoped<IRoleServices, RoleServices>();
builder.Services.AddScoped<IAccountServices, AccountServices>();

builder.Services.AddAutoMapper(typeof(Mapper));

builder.Services.AddPaging(options =>
{
    options.ViewName = "Bootstrap4";
});


builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllerRoute
               (name: "login",
                pattern: "/login",
                defaults: new { area = "Admin", controller = "Account", action = "Login" });
    endpoints.MapControllerRoute
    (
        name: "posts",
        pattern: "Post/{year}/{month}/{url}",
        defaults: new { controller = "Post", action = "Details" },
        constraints: new { year = @"\d{4}", month = @"\d{2}" }
    );
    endpoints.MapControllerRoute
               (name: "category",
                pattern: "/categories",
                defaults: new { area = "Admin", controller = "Categories", action = "Index" });

    endpoints.MapControllerRoute
                (name: "Denied",
                 pattern: "/access-denied",
                 defaults: new { area = "Admin", controller = "Account", action = "AccessDenied" });
    endpoints.MapControllerRoute(
      name: "areas",
      pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}"
    );
    endpoints.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
});



app.Run();
