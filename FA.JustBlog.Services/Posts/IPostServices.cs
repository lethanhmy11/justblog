﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.Posts;
using FA.JustBlog.ViewModels.ResponseResults;
using System.Collections.Generic;


namespace FA.JustBlog.Services.Posts
{
    public interface IPostServices
    {
        ResponseResult<CreatePostViewModel> Create(CreatePostViewModel request);
        ResponseResult<CreatePostViewModel> Update(CreatePostViewModel request);
        ResponseResult<int> Delete(int id);
        List<PostViewModel> GetAll();
        Post Find(int id);
        Post FindPost(int year, int month, string urlSlug);
        List<PostReviewViewModel> GetAllPostReview();
        List<PostReviewViewModel> GetMostViewedPost(int size);
        List<PostReviewViewModel> GetHighestPosts(int size);
        List<PostReviewViewModel> GetPostsByCategory(string category);
        List<PostReviewViewModel> GetPostsByTag(string tag);
        List<string> GetTagNameOfPost(int id);
        string StringTagOfPost(int id);
        string CategoryNameByPost(int id);
        List<PostViewModel> Search(string sreach);
    }
}
