﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Configurations
{
    public class UserRoleSeedConfiguration : IEntityTypeConfiguration<IdentityUserRole<string>>
    {
        public void Configure(EntityTypeBuilder<IdentityUserRole<string>> builder)
        {
            builder.HasData(
               new IdentityUserRole<string>
               {
                   RoleId = "14997052-bd22-4a73-98e0-1bba5efe2d2c",
                   UserId = "70dc1302-cf04-4f4e-83d7-da3eadd87082",
               },
               new IdentityUserRole<string>
               {
                   RoleId = "1f45ab0a-c177-42da-b07d-671387038d8b",
                   UserId = "7039c9ef-59be-4801-8ee7-e3b359d292f1",
               },
               new IdentityUserRole<string>
               {
                   RoleId = "f2049908-8a39-4236-bbcc-109f03ced012",
                   UserId = "a91119a9-9d47-4567-b2c9-2f9d941e0a25",
               }
           );
        }
    }
}
