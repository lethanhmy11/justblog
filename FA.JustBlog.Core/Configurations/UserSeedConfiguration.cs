﻿using FA.JustBlog.Core.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.JustBlog.Core.Configurations
{
    public class UserSeedConfiguration : IEntityTypeConfiguration<AppUser>
    {
        public void Configure(EntityTypeBuilder<AppUser> builder)
        {
            var hasher = new PasswordHasher<AppUser>();
            builder.HasData(
                new AppUser
                {
                    Id = "70dc1302-cf04-4f4e-83d7-da3eadd87082",
                    Email = "BlogOwen@gmail.com",
                    NormalizedEmail = "BLOGOWEN@GMAIL.COM",
                    NormalizedUserName = "BLOGOWEN",
                    UserName = "BlogOwen",
                    PasswordHash = hasher.HashPassword(null, "Admin123"),
                    EmailConfirmed = true
                },
                new AppUser
                {
                    Id = "7039c9ef-59be-4801-8ee7-e3b359d292f1",
                    Email = "contributor@gmail.com",
                    NormalizedEmail = "CONTRIBUTOR@GMAIL.COM",
                    NormalizedUserName = "CONTRIBUTOR",
                    UserName = "Contributor",
                    PasswordHash =hasher.HashPassword(null, "Admin123"),
                    EmailConfirmed = true
                },
                new AppUser
                {
                    Id = "a91119a9-9d47-4567-b2c9-2f9d941e0a25",
                    Email = "user@gmail.com",
                    NormalizedEmail = "USER@GMAIL.COM",
                    NormalizedUserName = "USER",
                    UserName = "User",
                    PasswordHash = hasher.HashPassword(null, "Admin123"),
                    EmailConfirmed = true
                }
            ); 
        }
    }
}
