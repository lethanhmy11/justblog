﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.ViewModels.Comments;
using FA.JustBlog.ViewModels.ResponseResults;
using System;
using System.Collections.Generic;


namespace FA.JustBlog.Services.Comments
{
    public class CommentServices : ICommentServices
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CommentServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public Comment AddComment(int PostId, string commenttext)
        {         
            var comment = new Comment()
            {
                PostId = PostId,
                CommentText = commenttext
            };
            unitOfWork.CommentResponsitory.Add(comment);
            unitOfWork.SaveChange();
            return comment;
            
        }

        public ResponseResult<CreateCommentViewModel> Create(CreateCommentViewModel request)
        {
            try
            {
                var comment = mapper.Map<Comment>(request);
                unitOfWork.CommentResponsitory.Add(comment);
                unitOfWork.SaveChange();
                return new ResponseResult<CreateCommentViewModel>();

            }catch(Exception ex)
            {
                return new ResponseResult<CreateCommentViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int id)
        {
            try
            {
                unitOfWork.CommentResponsitory.Delete(id);
                unitOfWork.SaveChange();
                return new ResponseResult<int>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public Comment Find(int id)
        {
            return unitOfWork.CommentResponsitory.Find(id);
        }

        public List<CommentViewModel> GetCommentsForPost(int id)
        {
            return mapper.Map<List<CommentViewModel>>(unitOfWork.CommentResponsitory.GetCommentsForPost(id));
        }

        public List<CommentViewUser> GetCommentsOfPostForUser(int id)
        {
            return mapper.Map<List<CommentViewUser>>(unitOfWork.CommentResponsitory.GetCommentsForPost(id));
        }

        public ResponseResult<CreateCommentViewModel> Update(CreateCommentViewModel request)
        {
            try
            {
                var comment = mapper.Map<Comment>(request);
                unitOfWork.CommentResponsitory.Update(comment);
                unitOfWork.SaveChange();
                return new ResponseResult<CreateCommentViewModel>();

            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateCommentViewModel>(ex.Message);
            }
        }
    }
}
