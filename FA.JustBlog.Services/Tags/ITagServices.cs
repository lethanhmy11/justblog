﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Tags;
using System.Collections.Generic;

namespace FA.JustBlog.Services.Tags
{
    public interface ITagServices
    {
        ResponseResult<UpdateTagViewModel> Update(UpdateTagViewModel tag);
        List<TagViewModel> GetAll();
        UpdateTagViewModel Find(int id);
        Tag FindDetails(int id);
        List<string> ListStringTagByCategory(string category);
    }
}
