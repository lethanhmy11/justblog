﻿using FA.JustBlog.Core.BaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Entities
{
    public class Comment:BaseEntity
    {
        public string? Name { set; get; }

        public string? Email { set; get; }

        public string? CommentHeader { set; get; }

        public string CommentText { set; get; }
        
        public int PostId { set; get; }

        public Post? Post { set; get; }

    }
}
