﻿using FA.JustBlog.Core.Enums;
using System;


namespace FA.JustBlog.ViewModels.Posts
{
    public class PostViewModel
    {
        public int Id { get; set; }

        public EntityStatus Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string Title { set; get; }
        
    }
}
