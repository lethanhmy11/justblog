﻿using System.Collections.Generic;
using System.Linq;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.Core.IReponsitories;

namespace FA.JustBlog.Core.Reponsitories
{
    public class PostTagMapReponsitory: GenericReponsitory<PostTagMap>, IPostTagMapReponsitory
    {
        private JustBlogContext context;

        public PostTagMapReponsitory(JustBlogContext context) : base(context)
        {
            this.context = context;
        }

        public bool CheckTagIsActice(int TagId)
        {
            var result = this.context.PostTagMaps.Where(p => p.TagId == TagId).ToList();
            if (result.Count() == 0)
            {
                return false;
            }
            else
                return true;
        }

        public bool CheckTagIsActiceByName(string tagNane)
        {
            var tag=this.context.Tags.FirstOrDefault(x=>x.Name==tagNane);
            if (tag != null)
            {
                var result = this.context.PostTagMaps.Where(p => p.TagId == tag.Id).ToList();
                if (result.Count() == 0)
                {
                    return false;
                }
                else
                    return true;
            }
            return false;
        }

        public PostTagMap GetPostTagMap(int idPost, int idTag)
        {
            return this.context.PostTagMaps.Where(p => p.PostId == idPost && p.TagId == idTag).FirstOrDefault();
        }

        public IList<PostTagMap> GetPostTagMapByPostId(int postid)
        {
            return this.context.PostTagMaps.Where(p => p.PostId == postid).ToList();
        }

        public void RemovePostTagMap(PostTagMap postTagMap)
        {
            this.context.PostTagMaps.Remove(postTagMap);
        }

        public void RemovePostTagMapByPostId(int PostId)
        {
            var postTagMap = this.context.PostTagMaps.Where(p => p.PostId == PostId).ToList();
            foreach (var item in postTagMap)
            {
                this.context.PostTagMaps.Remove(item);
            }
        }
    }
}

