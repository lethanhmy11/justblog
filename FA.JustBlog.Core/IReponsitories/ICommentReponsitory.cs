﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using System.Collections.Generic;

namespace FA.JustBlog.Core.IReponsitories
{
    public interface ICommentReponsitory:IGenericReponsitory<Comment>
    {
        void AddComment(int postId, string commentName, string commentEmail, string commentTitle,string commentBody);
        IList<Comment> GetCommentsForPost(int postId);
        IList<Comment> GetCommentsForPost(Post post);
    }
}
