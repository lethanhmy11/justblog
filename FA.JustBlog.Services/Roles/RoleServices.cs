﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Roles;
using Microsoft.AspNetCore.Identity;


namespace FA.JustBlog.Services.Roles
{
    public class RoleServices : IRoleServices
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<AppUser> userManager;

        public RoleServices(RoleManager<IdentityRole> roleManagers, UserManager<AppUser> userManager)
        {
            this.roleManager = roleManagers;
            this.userManager = userManager;
        }
        public async Task<ResponseResult<string>> CreateAsync(string roleNames)
        {
            try
            {              
                var identityRole = new IdentityRole() { Name = roleNames };
                var result = await roleManager.CreateAsync(identityRole);
                if (!result.Succeeded)
                       return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public async Task<ResponseResult<string>> DeleteAsync(string roleId)
        {
            try
            {
                IdentityRole role = await roleManager.FindByIdAsync(roleId);
                if (role != null)
                {
                    IdentityResult result = await roleManager.DeleteAsync(role);
                    if (!result.Succeeded)
                        return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                }
                else
                    return new ResponseResult<string>("Roles is not exists");
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public async Task<RoleEdit> FindRoleEdit(string id)
        {
            IdentityRole role = await roleManager.FindByIdAsync(id);
            List<AppUser> members = new List<AppUser>();
            List<AppUser> nonMembers = new List<AppUser>();
            foreach (AppUser user in userManager.Users)
            {
               
                bool isMember = await userManager.IsInRoleAsync(user, role.Name);
                if(isMember)
                    members.Add(user);
               else
                    nonMembers.Add(user);
            }

            return new RoleEdit
            {
                Role = role,
                Members = members,
                NonMembers = nonMembers
            };


            //IdentityRole role = await roleManager.FindByIdAsync(id);
            //List<AppUser> members = new List<AppUser>();
            //List<AppUser> nonMembers = new List<AppUser>();
            //foreach (AppUser user in userManager.Users)
            //{
            //    var list = await userManager.IsInRoleAsync(user, role.Name) ? members : nonMembers.ToList();
            //    list.Add(user);
            //}
            //return new RoleEdit
            //{
            //    Role = role,
            //    Members = members,
            //    NonMembers = nonMembers
            //};
        }

        public IEnumerable<IdentityRole> GetAll()
        {
            return roleManager.Roles;
        }

        public async Task<ResponseResult<string>> UpdateAsync(RoleModification model)
        {
            try
            {
                IdentityResult result;
                foreach (string userId in model.AddIds ?? new string[] { })
                {
                    AppUser user = await userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        result = await userManager.AddToRoleAsync(user, model.RoleName);
                        if (!result.Succeeded)
                            return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                    }
                }
                foreach (string userId in model.DeleteIds ?? new string[] { })
                {
                    AppUser user = await userManager.FindByIdAsync(userId);
                    if (user != null)
                    {
                        result = await userManager.RemoveFromRoleAsync(user, model.RoleName);
                        if (!result.Succeeded)
                            return new ResponseResult<string>(result.Errors.Select(x => x.Description).ToArray());
                    }

                }
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }
    }
}
