﻿using FA.JustBlog.Core.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Entities
{
    public class Post : BaseEntity
    {
        public string Title { get; set; }

        public string ShortDescription { get; set; }

        public string PostContent { get; set; }

        public string UrlSlug { get; set; }

        public bool Published { get; set; }

        public int CategoryId { get; set; }

        public virtual Category Category { set; get; }

        public virtual ICollection<PostTagMap> PostTagMaps { set; get; }


        public int ViewCount { set; get; }

        public int RateCount { set; get; }

        public int TotalRate { set; get; }

        public double Rate {set;get;}

        public virtual ICollection<Comment> Comments { set; get; }

    }
}
