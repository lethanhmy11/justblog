﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.Services.Accounts;
using FA.JustBlog.ViewModels.Accounts;
using FA.JustBlog.ViewModels.Users;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Web.Areas.Admin.Controllers
{
    [Area(areaName: "Admin")]
    public class AccountController :Controller
    {
        private readonly IAccountServices accountServices;
        private readonly UserManager<AppUser> userManager;

        public AccountController(IAccountServices accountServices, UserManager<AppUser> userManager)
        {
            this.accountServices = accountServices;
            this.userManager = userManager;
        }
        

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel request)
        {
            if (!ModelState.IsValid)
                return View(request);
            var response = await accountServices.SignInAsync(request);
            if (response.IsSuccessed)
                return Redirect("/Admin");
            ViewBag.ErrorMessages = response.ErrrorMessages;
            return View(request);
        }

        public async Task<IActionResult> Logout()
        {
            await accountServices.SingOutAsync();
            return RedirectToAction("Login", "Account");
        }

        [HttpGet]
        public IActionResult UpdateInfor(string id)
        {
            return View(accountServices.FindUpdateUserViewModel(id));
        }
        [HttpPost]
        public async Task<IActionResult> UpdateInfor(UpdateUserViewModel request)
        {
            if (!ModelState.IsValid)
                return View(request);
            var response = await accountServices.UpdateAsync(request);
            //if (response.IsSuccessed)
            //{
            //    return RedirectToAction(nameof(Index));
            //}

            ViewBag.ErrorMessages = response.ErrrorMessages;
            return View(request);
        }

        public IActionResult ChangePassword(string email)
        {
            var model = new ChangePassword { Email = email };
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(ChangePassword resetPassword)
        {
            if (!ModelState.IsValid)
            {
                return View(resetPassword);
            }
            var response = await accountServices.ResetPassword(resetPassword);
            //if (response.IsSuccessed)
            //{
            //    return RedirectToAction(nameof(Index));
            //}

            ViewBag.ErrorMessages = response.ErrrorMessages;
            return View(resetPassword);
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

    }
}
