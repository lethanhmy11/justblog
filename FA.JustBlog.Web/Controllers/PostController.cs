﻿using FA.JustBlog.Services.Comments;
using FA.JustBlog.Services.Posts;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Web.Controllers
{
    public class PostController : Controller
    {
        private readonly IPostServices postServices;
        private readonly ICommentServices commentServices;

        public PostController(IPostServices postServices, ICommentServices commentServices)
        {
            this.postServices = postServices;
            this.commentServices = commentServices;
        }

        
        public IActionResult Details( int year, int month, string url)
        {
            var post = postServices.FindPost(year, month, url);
            ViewBag.ListTag = postServices.GetTagNameOfPost(post.Id);
            var categoryName = postServices.CategoryNameByPost(post.Id);
            post.Category.Name = categoryName;
            return View(post);
        }

        public IActionResult GetPostByTag(string name)
        {
            ViewBag.TagName = name;
            return View(postServices.GetPostsByTag(name));
        }

        
        public JsonResult AddComment(int PostId, string CommentText)
        {
            var comment=commentServices.AddComment(PostId, CommentText);
            return Json(comment);
        }
    }
}
