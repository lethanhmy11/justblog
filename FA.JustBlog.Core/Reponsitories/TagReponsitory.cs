﻿using FA.JustBlog.Core.Commons;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.Core.IReponsitories;
using System.Collections.Generic;
using System.Linq;


namespace FA.JustBlog.Core.Reponsitories
{
    public class TagReponsitory : GenericReponsitory<Tag>, ITagReponsitory
    {
        private readonly JustBlogContext context;

        public TagReponsitory(JustBlogContext context) : base(context)
        {
            this.context = context;
        }

        public Tag GetTagByUrlSlug(string urlSlug)
        {
            return this.context.Tags.Where(t=>t.UrlSLug==urlSlug).FirstOrDefault();
        }

        public IEnumerable<int> AddTagByString(string tags)
        {
            if (tags != null)
            {
                var tagNames = tags.Split(';');
                foreach (var tagName in tagNames)
                {
                    var tagExisting = this.context.Tags.Where(t => t.Name.Trim().ToLower() == tagName.Trim().ToLower()).ToList();
                    if (tagExisting.Count == 0)
                    {
                        var tag = new Tag()
                        {
                            Name = tagName,
                            UrlSLug = UrlHelper.FrientlyUrl(tagName)
                        };
                        this.dbSet.Add(tag);
                    }
                }
                this.context.SaveChanges();

                foreach (var tagName in tagNames)
                {
                    var tagExisting = this.dbSet.FirstOrDefault(t => t.Name.Trim().ToLower() == tagName.Trim().ToLower());
                    if (tagExisting != null)
                    {
                        yield return tagExisting.Id;
                    }
                }
            }
        }

        public string ListStringTag(int postId)
        {
            string listStringTag = "";
            var list = this.context.Tags.Join(this.context.PostTagMaps, a => a.Id, b => b.TagId, (a, b) => new
            {
                TagName = a.Name,
                PostId = b.PostId
            }).Where(c => c.PostId == postId).ToList();
            foreach (var tag in list)
            {
                listStringTag += tag.TagName + ";";
            }
            if (listStringTag != "")
            {
                if (listStringTag.Substring(listStringTag.Length - 1) == ";")
                {
                    listStringTag = listStringTag.Substring(0, listStringTag.Length - 1);
                }
            }

            return listStringTag;
        }

        public Tag GetTagByName(string name)
        {
            return this.context.Tags.Where(t => t.Name == name).FirstOrDefault();
        }

        public List<string> ListStringTagByCategory(string category)
        {
            string listTag="";
            var find=this.context.Categories.Where(p=>p.Name== category).FirstOrDefault();
            var postOfCategory = this.context.Posts.Where(p => p.CategoryId == find.Id).ToList();           
            foreach (var item in postOfCategory)
            {
                var listStringTagOfPost = ListStringTag(item.Id);
                listTag = listTag + ";" + listStringTagOfPost;
            }
            var tags = new List<string>();
            if (!String.IsNullOrEmpty(listTag))
            {
                listTag = listTag.Substring(1, listTag.Length - 1);
                var arrayTags = listTag.Split(";");
                
                for (int i = 0; i < arrayTags.Length; i++)
                {
                    tags.Add(arrayTags[i]);
                }
                tags = tags.Distinct().ToList();
            }
            return tags;

        }
    }
}
