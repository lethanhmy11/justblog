﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.Categories;
using FA.JustBlog.ViewModels.Posts;
using FA.JustBlog.ViewModels.ResponseResults;
using System.Collections.Generic;


namespace FA.JustBlog.Services.Categories
{
    public interface ICategoryServices
    {
        ResponseResult<CreateCategoryViewModel> Create(CreateCategoryViewModel request);
        ResponseResult<CreateCategoryViewModel> Update(CreateCategoryViewModel request);
        ResponseResult<int> Delete(int id);
        List<CategoryViewModel> GetAll();
        Category Find(int id);
    }
}
