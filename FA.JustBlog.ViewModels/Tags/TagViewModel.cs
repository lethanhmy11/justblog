﻿using FA.JustBlog.Core.Enums;
using System;


namespace FA.JustBlog.ViewModels.Tags
{
    public class TagViewModel
    {
        public int Id { get; set; }

        public EntityStatus Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public string Name { set; get; }

        public string UrlSLug { get; set; }
    }
}
