﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.Core.IReponsitories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Reponsitories
{
    public class CommentReponsitory : GenericReponsitory<Comment>, ICommentReponsitory
    {
        private readonly JustBlogContext context;

        public CommentReponsitory(JustBlogContext context) : base(context)
        {
            this.context = context;
        }

        public void AddComment(int postId, string commentName, string commentEmail, string commentTitle, string commentBody)
        {
            Comment comment = new Comment();
            comment.Name = commentName;
            comment.CommentHeader = commentTitle;
            comment.CommentText = commentBody;
            comment.Email = commentEmail;
            comment.PostId = postId;
            this.context.Add(comment);
        }

        public IList<Comment> GetCommentsForPost(int postId)
        {
            return this.context.Comments.Where(c => c.PostId == postId).ToList();
        }

        public IList<Comment> GetCommentsForPost(Post post)
        {
            return this.context.Comments.Where(c => c.PostId == post.Id).ToList();
        }
    }
}
