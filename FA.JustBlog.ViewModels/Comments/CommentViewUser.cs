﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.ViewModels.Comments
{
    public class CommentViewUser
    {
        public string Name { set; get; }

        public string CommentText { set; get; }

        public DateTime ModifiedOn { get; set; }
    }
}
