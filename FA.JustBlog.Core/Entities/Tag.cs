﻿using FA.JustBlog.Core.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Entities
{
    public class Tag:BaseEntity
    {       
        public string Name { get; set; }

        public string UrlSLug { get; set; }

        public string? Description { get; set; }

        public int Count { get; set; }

        public virtual ICollection<PostTagMap> PostTagMaps { set; get; }
    }
}
