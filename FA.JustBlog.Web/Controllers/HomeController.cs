﻿using FA.JustBlog.Services.Categories;
using FA.JustBlog.Services.Posts;
using FA.JustBlog.Services.Tags;
using FA.JustBlog.Web.Models;
using Microsoft.AspNetCore.Mvc;
using ReflectionIT.Mvc.Paging;
using System.Diagnostics;

namespace FA.JustBlog.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IPostServices postServices;
        private readonly ICategoryServices categoryServices;

        public HomeController(IPostServices postServices, ICategoryServices categoryServices)
        {
            this.postServices = postServices;
            this.categoryServices = categoryServices;
           
        }

        public IActionResult Index(int pageIndex=1)
        {
          
            var posts = postServices.GetAllPostReview().OrderBy(p => p.ModifiedOn);
            var model = PagingList.Create(posts, 3, pageIndex);
            return View(model);
        }

        public IActionResult Details(int id)
        {
            ViewBag.ListTag = postServices.GetTagNameOfPost(id);
            var post = postServices.Find(id);
            var categoryName= postServices.CategoryNameByPost(id);
            post.Category.Name = categoryName;
            return View(post);
        }


        //public ActionResult Menu()
        //{
        //    return PartialView("_Menu", categoryServices.GetAll());
        //}


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}