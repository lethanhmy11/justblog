﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.ViewModels.Comments
{
    public class CreateCommentViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Comment Header")]
        [Required(ErrorMessage = "Comment Header is required")]
        public string CommentHeader { set; get; }

        [Display(Name = "CommentText")]
        [Required(ErrorMessage = "Comment Text is required")]
        public string CommentText { set; get; }

        public int PostId { get; set; }

        public string Name { set; get; }

        public string Email { set; get; }
    }
}
