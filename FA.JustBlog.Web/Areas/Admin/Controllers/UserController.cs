﻿using FA.JustBlog.Services.Roles;
using FA.JustBlog.Services.Users;
using FA.JustBlog.ViewModels.Users;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Web.Areas.Admin.Controllers
{
    public class UserController : BaseController
    {
        private readonly IUserServices userService;
        private readonly IRoleServices roleService;

        public UserController(IUserServices userServices, IRoleServices roleServices)
        {
            userService = userServices;
            roleService = roleServices;
        }

        public IActionResult Index()
        {
            return View(userService.GetAll());
        }

        public IActionResult Details(string id)
        {
            return View(userService.Find(id));
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(CreateUserViewModel request)
        {
            if (!ModelState.IsValid)
                return View(request);
            var response = await userService.CreateAsync(request);
            if (response.IsSuccessed)
            {
                return RedirectToAction(nameof(Index));
            }

            ViewBag.ErrorMessages = response.ErrrorMessages;
            return View(request);
        }

        [HttpGet]
        public IActionResult Update(string id)
        {
            return View(userService.FindUpdateUserViewModel(id));
        }
        [HttpPost]
        public async Task<IActionResult> Update(UpdateUserViewModel request)
        {
            if (!ModelState.IsValid)
                return View(request);
            var response = await userService.UpdateAsync(request);
            if (response.IsSuccessed)
            {
                return RedirectToAction(nameof(Index));
            }

            ViewBag.ErrorMessages = response.ErrrorMessages;
            return View(request);
        }

        public IActionResult Delete(string id)
        {
            return View(userService.Find(id));
        }

        [HttpPost, ActionName("Delete")]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            await userService.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

    }
}

