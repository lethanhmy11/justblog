﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Services.Users
{
    public interface IUserServices
    {
        Task<ResponseResult<UserViewModel>> CreateAsync(CreateUserViewModel request);
        Task<ResponseResult<UserViewModel>> UpdateAsync(UpdateUserViewModel request);
        Task<ResponseResult<UserViewModel>> DeleteAsync(string id);
        Task<ResponseResult<UserViewModel>> SignInAsync(LoginUserViewModel request);
        Task<ResponseResult<UserViewModel>> RoleAssignAsync(string email, params string[] roleNames);
        List<UserViewModel> GetAll();
        AppUser Find(string id);
        UpdateUserViewModel FindUpdateUserViewModel(string id);
        //Task<ResponseResult<AppUser>> GetCurrentUserAsync();

    }
}
