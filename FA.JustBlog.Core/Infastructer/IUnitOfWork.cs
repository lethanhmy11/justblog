﻿using FA.JustBlog.Core.IReponsitories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Infastructer
{
    public interface IUnitOfWork:IDisposable
    {
        ICategoryReponsitory CategoryResponsitory { get; }

        IPostReponsitory PostResponsitory { get; }

        ITagReponsitory TagResponsitory { get; }

        IPostTagMapReponsitory PostTagMapResponsitory { get; }

        ICommentReponsitory CommentResponsitory { get; }

        int SaveChange();
        
        Task<int> SaveChangesAsync();
    }
}
