﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using FA.JustBlog.Services.Posts;
using FA.JustBlog.Services.Categories;
using FA.JustBlog.ViewModels.Posts;
using AutoMapper;
using FA.JustBlog.Web.Areas.Admin.Views.Shared.Components.SearchBar;


namespace FA.JustBlog.Web.Areas.Admin.Controllers
{
    
    public class PostController : BaseController
    {
        private readonly IPostServices postServices;
        private readonly ICategoryServices categoryServices;
        private readonly IMapper mapper;

        public PostController(IPostServices postServices, ICategoryServices categoryServices, IMapper mapper)
        {
            this.postServices = postServices;
            this.categoryServices = categoryServices;
            this.mapper = mapper;
        }

        public IActionResult Index(string Search = "", int pg = 1)
        {
            var posts = new List<PostViewModel>();
            if (Search != null && Search != "")
            {
                posts = postServices.Search(Search);
            }
            else
            {
                posts = postServices.GetAll();
            }
            const int pageSize = 10;
            int recsCount = posts.Count();
            int resSkip = (pg - 1) * pageSize;
            List<PostViewModel> resPost = posts.Skip(resSkip).Take(pageSize).ToList();
            SPager SearchPager = new SPager(recsCount, pg, pageSize)
            {
                Action = "Index",
                Controller = "Post",
                Search = Search
            };
            ViewBag.SearchPager = SearchPager;
            return View(resPost);
        }


        public IActionResult Details(int id)
        {
            var post = postServices.Find(id);
            if (post == null)
            {
                return NotFound();
            }
            return View(post);
        }

        public IActionResult Create()
        {
            var post = new CreatePostViewModel();
            ViewData["CategoryId"] = new SelectList(categoryServices.GetAll(), "Id", "Name");
            return View(post);
        }

        [HttpPost]
        public IActionResult Create(CreatePostViewModel post)
        {
            if (ModelState.IsValid)
            {
                postServices.Create(post);
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(categoryServices.GetAll(), "Id", "Name");
            return View(post);
        }

        public IActionResult Edit(int id)
        {
            var post = postServices.Find(id);
            if (post == null)
            {
                return NotFound();
            }
            ViewData["CategoryId"] = new SelectList(categoryServices.GetAll(), "Id", "Name", post.CategoryId);
            var model = mapper.Map<CreatePostViewModel>(post);
            model.Tags = postServices.StringTagOfPost(id);
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(CreatePostViewModel post)
        {
            if (ModelState.IsValid)
            {              
                postServices.Update(post);  
                return RedirectToAction(nameof(Index));
            }
            ViewData["CategoryId"] = new SelectList(categoryServices.GetAll(), "Id", "Name", post.CategoryId);
            return View(post);
        }

        public IActionResult Delete(int id)
        {
            var post = postServices.Find(id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id)
        {
            var post = postServices.Delete(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
