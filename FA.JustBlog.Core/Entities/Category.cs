﻿using FA.JustBlog.Core.BaseEntities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Entities
{
    public class Category : BaseEntity
    {
        [Required(ErrorMessage = "Category name is required")]
        [StringLength(255)]
        public string Name { get; set; }

        [Required(ErrorMessage = "UrlSLug is required")]
        [StringLength(255)]
        public string UrlSLug { get; set; }

        [Required(ErrorMessage = "Description name is required")]
        [StringLength(1024)]
        public string Description { get; set; }

        public virtual ICollection<Post> Posts { set; get; }
    }
}
