﻿using FA.JustBlog.Core.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core
{
    public static class ModelBuilderExtension
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Category>().HasData(
                new Category { Id = 1, Name = "SQL", UrlSLug="sql", Description = "Description 1", CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable},
                new Category { Id = 2, Name = "Entity FrameWork", UrlSLug = "entityframework", Description = "Description 1", CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable },
                new Category { Id = 3, Name = "ASP.NET Core", UrlSLug = "mvc", Description = "Description 1", CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable }
            );
            modelBuilder.Entity<Tag>().HasData(
               new Tag { Id = 1, Name = "SQL", UrlSLug = "sql", Description= "Description 1", Count = 2 , CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable },
               new Tag { Id = 2, Name = "JavaScript", UrlSLug = "javascript", Description = "Description 1", Count = 2, CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable },
               new Tag { Id = 3, Name = "NPL", UrlSLug = "npl", Description = "Description 1", Count = 2, CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable }
           ); 
            modelBuilder.Entity<Post>().HasData(
               new Post { Id = 1, Title = "SQL Server - Functions", ShortDescription= "Functions in SQL Server are similar to functions in other programming languages. Functions in SQL Server contains SQL statements that perform some specific tasks.", 
                   PostContent= "Types of Functions SQL Server Functions are of two types:System Functions: These are built -in functions available in every database.Some common types are Aggregate functions," +
                   "Analytic functions,Ranking functions,Rowset functions,Scalar functions.User Defined Functions(UDFs): Functions created by " +
                   "the database user are called User - defined functions.UDFs are of two types",
                   UrlSlug="sql-server-function", Published=true, CategoryId=1, ViewCount=12500, RateCount=62, TotalRate=250, Rate= 4.03, CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable },
               
               new Post { Id = 2, Title = "SQL Server Indexes", ShortDescription = "Culstered index and Non-Clustered idex", 
               PostContent = "An Index in SQL Server is a data structure associated with tables and views that helps in faster retrieval of rows." +
               "Data in a table is stored in rows in an unordered structure called Heap.If you have to fetch data from a table" +
               "the query optimizer has to scan the entire table to retrieve the required row(s).If a table has a large number of rows," +
               "then SQL Server will take a long time to retrieve the required rows.So,to speed up data retrieval,SQL Server has a special data structure called indexes.",
               UrlSlug = "sql-server-index", Published = true, CategoryId = 1, ViewCount = 12500, RateCount = 62, TotalRate = 250, Rate = 4.03, CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable },
               
               new Post { Id = 3, Title = "SQL Server Data Types", ShortDescription = "In SQL Server, data type specifies the type of data that can be stored in a column of a table such as integer data, string data, date & time, binary strings, etc.",
               PostContent = "In SQL Server, data type specifies the type of data that can be stored in a column of a table such as integer data, string data, date & time, binary strings, etc." +
               "Assigning an appropriate data type to columns in a table is crucial while designing a database. It affects the performance and efficiency of the database and the application using the database." +
               "SQL Server provides built-in data types for all kinds of data that can be used within SQL Server.Additionally, you can also define your own data type in T - SQL.",
               UrlSlug = "sql-server-datatype", Published = true, CategoryId = 1, ViewCount = 12500, RateCount = 62, TotalRate = 250, Rate = 4.03, CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable },
               
               new Post { Id = 4, Title = "Install .NET Core, ASP.NET Core", ShortDescription = "Here you will learn to prepare a development environment for building .NET Core/ASP.NET Core applications.", 
               PostContent = "Here you will learn to prepare a development environment for building .NET Core/ASP.NET Core applications.NET Core can be installed in two ways: By installing Visual Studio 2017 / 2019 or by installing.NET Core Runtime or SDK..NET Core " +
               "installer already contains ASP.NET Core libraries,so there is no separate installer for ASP.NET Core.Install Visual Studio " +
               "Currently, .NET Core 2.1 and.NET Core 3.1 is having long term support.Visual Studio 2017 supports.NET Core 2.1, whereas Visual Studio 2019 supports both the versions." +
               "You can use your favorite IDE, such as Visual Studio, Visual Studio Code, Sublime Text, etc.to develop, restore, build, and run.NET Core application.Here, we will use Visual Studio 2019." +
               "If you don't have Visual Studio on your development PC, then it is recommended to install the latest Visual Studio 2019. If you already have either Visual Studio 2017 or 2019, then you already have installed .NET Core 2.1." +
               "Download and install Visual Studio 2019 based on your OS from here.Select the appropriate edition as per your license.The community edition is free for students, open - source contributors, and individuals." +
               "During installation, select.NET Core cross-platform development, workload.This will install.NET Core 2.1.However, you need to install.NET Core 3.1 SDK separately.",
               UrlSlug = "install-net-core", Published = true, CategoryId = 3, ViewCount = 12500, RateCount = 62, TotalRate = 250, Rate = 4.03, CreatedOn = new DateTime(2020, 12, 25), ModifiedOn = new DateTime(2021, 01, 10), Status = Enums.EntityStatus.Enable },

               new Post
               {
                   Id = 5,
                   Title = "ASP.NET Core Overview",
                   ShortDescription = "New version of the ASP.NET web framework",
                   PostContent = "ASP.NET Core is the new version of the ASP.NET web framework mainly targeted to run on .NET Core platform.ASP.NET Core is a free",
                   UrlSlug = "asp-netcore-overview",
                   Published = true,
                   CategoryId = 3,
                   ViewCount = 12500,
                   RateCount = 62,
                   TotalRate = 250,
                   Rate = 4.25,
                   CreatedOn = new DateTime(2020, 12, 25),
                   ModifiedOn = new DateTime(2021, 01, 10),
                   Status = Enums.EntityStatus.Enable
               },

               new Post
               {
                   Id = 6,
                   Title = "ChangeTracker in Entity Framework Core",
                   ShortDescription = "The DbContext in Entity Framework Core includes the ChangeTracker class in Microsoft.EntityFrameworkCore.ChangeTracking namespace which is responsible of tracking the state of each entity retrieved using the same DbContext instance. It is not intended to use it directly in your application code because it may change in future versions. However, you can use some methods for tracking purpose.",
                   PostContent = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, neque ducimus accusamus eaque doloremque sunt explicabo tempora ratione perspiciatis cumque id ab repudiandae necessitatibus libero praesentium? Ad dolorem aperiam expedita?",
                   UrlSlug = "asp-netcore-overview",
                   Published = true,
                   CategoryId = 2,
                   ViewCount = 10500,
                   RateCount = 62,
                   TotalRate = 250,
                   Rate = 4.5,
                   CreatedOn = new DateTime(2020, 12, 25),
                   ModifiedOn = new DateTime(2021, 01, 10),
                   Status = Enums.EntityStatus.Enable
               },

               new Post
               {
                   Id = 7,
                   Title = "Fluent API in Entity Framework Core",
                   ShortDescription = "New version of the ASP.NET web framework",
                   PostContent = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, neque ducimus accusamus eaque doloremque sunt explicabo tempora ratione perspiciatis cumque id ab repudiandae necessitatibus libero praesentium? Ad dolorem aperiam expedita?",
                   UrlSlug = "asp-netcore-overview",
                   Published = true,
                   CategoryId = 2,
                   ViewCount = 100,
                   RateCount = 62,
                   TotalRate = 250,
                   Rate = 4.03,
                   CreatedOn = new DateTime(2020, 12, 25),
                   ModifiedOn = new DateTime(2021, 01, 10),
                   Status = Enums.EntityStatus.Enable
               },


               new Post
               {
                   Id = 8,
                   Title = "Migration in Entity Framework Core",
                   ShortDescription = "New version of the ASP.NET web framework",
                   PostContent = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, neque ducimus accusamus eaque doloremque sunt explicabo tempora ratione perspiciatis cumque id ab repudiandae necessitatibus libero praesentium? Ad dolorem aperiam expedita?",
                   UrlSlug = "asp-netcore-overview",
                   Published = true,
                   CategoryId = 2,
                   ViewCount = 5500,
                   RateCount = 62,
                   TotalRate = 250,
                   Rate = 2.03,
                   CreatedOn = new DateTime(2020, 12, 25),
                   ModifiedOn = new DateTime(2021, 01, 10),
                   Status = Enums.EntityStatus.Enable
               },


               new Post
               {
                   Id = 9,
                   Title = "Entity Framework Core: DbContext",
                   ShortDescription = "New version of the ASP.NET web framework",
                   PostContent = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, neque ducimus accusamus eaque doloremque sunt explicabo tempora ratione perspiciatis cumque id ab repudiandae necessitatibus libero praesentium? Ad dolorem aperiam expedita?",
                   UrlSlug = "asp-netcore-overview",
                   Published = true,
                   CategoryId = 2,
                   ViewCount = 20500,
                   RateCount = 62,
                   TotalRate = 250,
                   Rate = 3.03,
                   CreatedOn = new DateTime(2020, 12, 25),
                   ModifiedOn = new DateTime(2021, 01, 10),
                   Status = Enums.EntityStatus.Enable
               },

               new Post
               {
                   Id = 10,
                   Title = "Dependency Injection",
                   ShortDescription = "New version of the ASP.NET web framework",
                   PostContent = "Lorem, ipsum dolor sit amet consectetur adipisicing elit. Officiis, neque ducimus accusamus eaque doloremque sunt explicabo tempora ratione perspiciatis cumque id ab repudiandae necessitatibus libero praesentium? Ad dolorem aperiam expedita?",
                   UrlSlug = "asp-netcore-overview",
                   Published = true,
                   CategoryId = 3,
                   ViewCount = 12500,
                   RateCount = 62,
                   TotalRate = 250,
                   Rate = 4.03,
                   CreatedOn = new DateTime(2020, 12, 25),
                   ModifiedOn = new DateTime(2021, 01, 10),
                   Status = Enums.EntityStatus.Enable
               }
           );

            modelBuilder.Entity<PostTagMap>().HasData(
                    new PostTagMap { PostId = 1, TagId = 1 },
                    new PostTagMap { PostId = 1, TagId = 2 },
                    new PostTagMap { PostId = 1, TagId = 3 },
                    new PostTagMap { PostId = 2, TagId = 1 },
                    new PostTagMap { PostId = 2, TagId = 3 },
                    new PostTagMap { PostId = 3, TagId = 1 },
                    new PostTagMap { PostId = 4, TagId = 2 },
                    new PostTagMap { PostId = 5, TagId = 2 },
                    new PostTagMap { PostId = 6, TagId = 1 },
                    new PostTagMap { PostId = 7, TagId = 3 },
                    new PostTagMap { PostId = 8, TagId = 1 },
                    new PostTagMap { PostId = 9, TagId = 2 },
                    new PostTagMap { PostId = 10, TagId = 2 }
                );

            modelBuilder.Entity<Comment>().HasData(
                   new Comment { 
                       Id=1,
                       Name="Quỳnh Trang",
                       Email="Quynhtrang@gmail.com",
                       CommentHeader="Rất hay",
                       CommentText="Bài viết này rất bổ ích với tôi", 
                       PostId=1,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 2,
                       Name = "Phạm Lan",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 1,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 3,
                       Name = "Minh Anh",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 2,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 4,
                       Name = "Tuấn Linh",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 2,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 5,
                       Name = "Hà Giang",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 3,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 6,
                       Name = "Phạm Quỳnh",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 3,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 7,
                       Name = "Quốc Tuấn",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 4,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 8,
                       Name = "Anh Dũng",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 4,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   },
                   new Comment
                   {
                       Id = 9,
                       Name = "Quách Thảo",
                       Email = "Quynhtrang@gmail.com",
                       CommentHeader = "Rất hay",
                       CommentText = "Bài viết này rất bổ ích với tôi",
                       PostId = 5,
                       CreatedOn = new DateTime(2020, 12, 25),
                       ModifiedOn = new DateTime(2021, 01, 10),
                       Status = Enums.EntityStatus.Enable
                   }

               );


        }
    }
}
