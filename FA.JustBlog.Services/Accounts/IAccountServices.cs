﻿using FA.JustBlog.ViewModels.Accounts;
using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Users;

namespace FA.JustBlog.Services.Accounts
{
    public interface IAccountServices
    {
        Task<ResponseResult<string>> SignInAsync(LoginViewModel request);
        Task<ResponseResult<string>> SingOutAsync();
        Task<ResponseResult<UserViewModel>> UpdateAsync(UpdateUserViewModel request);
        UpdateUserViewModel FindUpdateUserViewModel(string id);
        Task<ResponseResult<ChangePassword>> ResetPassword(ChangePassword request);
    }
}
