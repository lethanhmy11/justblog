﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Services.Comments;
using FA.JustBlog.ViewModels.Comments;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Web.Areas.Admin.Controllers
{
    public class CommentController : BaseController
    {
        private readonly ICommentServices commentServices;
        private readonly IMapper mapper;
        private readonly UserManager<AppUser> userManager;

        public CommentController(ICommentServices commentServices, IMapper mapper, UserManager<AppUser> userManager)
        {
            this.commentServices = commentServices;
            this.mapper = mapper;
            this.userManager = userManager;
        }

        public IActionResult Index(int id)
        {
            ViewBag.PostId = id;
            return View(commentServices.GetCommentsForPost(id));
        }

        [HttpGet]
        public IActionResult Create(int postId)
        {
            ViewBag.PostId = postId;
            CreateCommentViewModel comment = new CreateCommentViewModel();
            return View(comment);
        }


        [HttpPost]
        public IActionResult Create(CreateCommentViewModel comment)
        {
            if (ModelState.IsValid)
            {
                commentServices.Create(comment);
                return RedirectToAction("Index", new {id=comment.PostId});
            }
            return View(comment);
        }


        public IActionResult Edit(int id, int postId)
        {
            ViewBag.PostId = postId;
            var comment = commentServices.Find(id);
            if (comment == null)
            {
                return NotFound();
            }
            return View(mapper.Map<CreateCommentViewModel>(comment));
        }


        [HttpPost]
        public IActionResult Edit(CreateCommentViewModel comment)
        {
            if (ModelState.IsValid)
            {
                commentServices.Update(comment);
                return RedirectToAction("Index", new { id = comment.PostId });
            }
            return View(comment);
        }


        public IActionResult Delete(int id, int postId)
        {
            ViewBag.PostId = postId;
            var category = commentServices.Find(id);
            if (category == null)
            {
                return NotFound();
            }
            return View(category);
        }


        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int id, int postId)
        {
            commentServices.Delete(id);
            return RedirectToAction("Index", new {id=postId});
        }

    }
}
