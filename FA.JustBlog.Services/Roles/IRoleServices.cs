﻿using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Roles;
using Microsoft.AspNetCore.Identity;


namespace FA.JustBlog.Services.Roles
{
    public interface IRoleServices
    {
        IEnumerable<IdentityRole> GetAll();
        Task<ResponseResult<string>> CreateAsync(string roleNames);
        Task<ResponseResult<string>> UpdateAsync(RoleModification model);
        Task<ResponseResult<string>> DeleteAsync(string roleId);
        Task<RoleEdit> FindRoleEdit(string id);
    }
}
