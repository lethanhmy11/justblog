﻿using FA.JustBlog.Core.Entities;
using Microsoft.AspNetCore.Identity;


namespace FA.JustBlog.ViewModels.Roles
{
    public class RoleEdit
    {
        public IdentityRole Role { get; set; }
        public IEnumerable<AppUser> Members { get; set; }
        public IEnumerable<AppUser> NonMembers { get; set; }
    }
}
