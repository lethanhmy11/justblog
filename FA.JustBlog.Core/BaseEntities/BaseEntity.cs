﻿using FA.JustBlog.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.BaseEntities
{
    public class BaseEntity : IBaseEntity
    {
        public int Id { get; set; }
        public EntityStatus Status { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }
    }
}
