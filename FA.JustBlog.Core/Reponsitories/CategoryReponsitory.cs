﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.Core.IReponsitories;


namespace FA.JustBlog.Core.Reponsitories
{
    public class CategoryReponsitory : GenericReponsitory<Category>, ICategoryReponsitory
    {
        private readonly JustBlogContext context;

        public CategoryReponsitory(JustBlogContext context) : base(context)
        {
            this.context = context;
        }
    }
}
