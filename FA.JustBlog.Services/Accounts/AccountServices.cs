﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.Accounts;
using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Users;
using Microsoft.AspNetCore.Identity;

namespace FA.JustBlog.Services.Accounts
{
    public class AccountServices : IAccountServices
    {
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly IUserValidator<AppUser> userValid;
        private readonly IMapper mapper;

        public AccountServices(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IUserValidator<AppUser> userValid, IMapper mapper)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.userValid = userValid;
            this.mapper = mapper;
        }

        public UpdateUserViewModel FindUpdateUserViewModel(string id)
        {
            return mapper.Map<UpdateUserViewModel>(userManager.Users.FirstOrDefault(p=>p.Id==id));
        }

        public async Task<ResponseResult<ChangePassword>> ResetPassword(ChangePassword request)
        {
            try
            {
                if(!request.NewPassword.Equals(request.ConfirmPassword))
                    return new ResponseResult<ChangePassword>("Confirm password is not correct");
                var user = await userManager.FindByEmailAsync(request.Email);
                var changePassResult = await userManager.ChangePasswordAsync(user, request.CurrentPassword, request.NewPassword);
                if (changePassResult.Succeeded)
                    return new ResponseResult<ChangePassword>("Change password successfully");
                return new ResponseResult<ChangePassword>(changePassResult.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<ChangePassword>(ex.Message);
            }
        }

        public async Task<ResponseResult<string>> SignInAsync(LoginViewModel request)
        {
            try
            {
                var userExisting = await userManager.FindByEmailAsync(request.Email);
                if (userExisting == null)
                    return new ResponseResult<string>($"Email: {request.Email} not exist");
                var result = await signInManager.PasswordSignInAsync(userExisting, request.Password, request.RememberMe, false);
                if (result.Succeeded)
                    return new ResponseResult<string>();
                if (result.IsLockedOut)
                    return new ResponseResult<string>("Account is locked");
                return new ResponseResult<string>("Login is not successfully");
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public async Task<ResponseResult<string>> SingOutAsync()
        {
            try
            {
                signInManager.SignOutAsync();
                return new ResponseResult<string>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<string>(ex.Message);
            }
        }

        public async Task<ResponseResult<UserViewModel>> UpdateAsync(UpdateUserViewModel request)
        {
            try
            {
                AppUser user = await userManager.FindByIdAsync(request.Id);
                user.UserName = request.UserName;
                user.PhoneNumber = request.PhoneNumber;
                IdentityResult validEmail = null;
                if (!string.IsNullOrEmpty(request.Email))
                {
                    validEmail = await userValid.ValidateAsync(userManager, user);
                    if (validEmail.Succeeded)
                        user.Email = request.Email;
                    else
                        return new ResponseResult<UserViewModel>("Email is invalid");
                }
                IdentityResult result = await userManager.UpdateAsync(user);
                if (result.Succeeded)
                    return new ResponseResult<UserViewModel>("Update Info Successfully");
                else
                    return new ResponseResult<UserViewModel>(result.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<UserViewModel>(ex.Message);
            }
        }
    }
}
