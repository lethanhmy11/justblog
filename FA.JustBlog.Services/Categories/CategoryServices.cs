﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.ViewModels.Categories;
using FA.JustBlog.ViewModels.ResponseResults;
using System;
using System.Collections.Generic;


namespace FA.JustBlog.Services.Categories
{
    public class CategoryServices : ICategoryServices
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public CategoryServices(IUnitOfWork unitOfWork,IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public ResponseResult<CreateCategoryViewModel> Create(CreateCategoryViewModel request)
        {
            try
            {
                var category = mapper.Map<Category>(request);
                this.unitOfWork.CategoryResponsitory.Add(category);
                this.unitOfWork.SaveChange();
                return new ResponseResult<CreateCategoryViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateCategoryViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int id)
        {
            try
            {
                this.unitOfWork.CategoryResponsitory.Delete(id);
                this.unitOfWork.SaveChange();
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public Category Find(int id)
        {
            return this.unitOfWork.CategoryResponsitory.Find(id);
        }

        public List<CategoryViewModel> GetAll()
        {
            return mapper.Map<List<CategoryViewModel>>(this.unitOfWork.CategoryResponsitory.GetAll());           
        }

        
        public ResponseResult<CreateCategoryViewModel> Update(CreateCategoryViewModel request)
        {
            try
            {
                var category = mapper.Map<Category>(request);
                this.unitOfWork.CategoryResponsitory.Update(category);
                this.unitOfWork.SaveChange();
                return new ResponseResult<CreateCategoryViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreateCategoryViewModel>(ex.Message);
            }
        }
    }
}
