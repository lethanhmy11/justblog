﻿using FA.JustBlog.Services.Roles;
using FA.JustBlog.ViewModels.Roles;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FA.JustBlog.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class RoleController : BaseController
    {
        private readonly IRoleServices roleServices;

        public RoleController(IRoleServices roleServices)
        {
            this.roleServices = roleServices;
        }

        public IActionResult Index()
        {
            return View(roleServices.GetAll());
        }


        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create([Required] string name)
        {
            if (ModelState.IsValid)
            {
                await roleServices.CreateAsync(name);
               return RedirectToAction(nameof(Index));
            }
            return View(name);
        }

        public async Task<IActionResult> Update(string id)
        {
            return View(await roleServices.FindRoleEdit(id));
        }

        [HttpPost]
        public async Task<IActionResult> Update(RoleModification model)
        {
            if (ModelState.IsValid)
            {
               await roleServices.UpdateAsync(model);
            }

            if (ModelState.IsValid)
                return RedirectToAction(nameof(Index));
            else
                return await Update(model.RoleId);
        }

        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            await roleServices.DeleteAsync(id);
            return RedirectToAction(nameof(Index));
        }

    }
}
