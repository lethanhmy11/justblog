﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.Categories;
using FA.JustBlog.ViewModels.Comments;
using FA.JustBlog.ViewModels.Posts;
using FA.JustBlog.ViewModels.Tags;
using FA.JustBlog.ViewModels.Users;

namespace FA.JustBlog.Services.Mappings
{
    public class Mapper:Profile
    {
        public Mapper()
        {
            CreateMap<Category, CreateCategoryViewModel>().ReverseMap();
            CreateMap<Category,CategoryViewModel>().ReverseMap();
            CreateMap<Post, CreatePostViewModel>().ReverseMap();
            CreateMap<Post, PostViewModel>().ReverseMap();
            CreateMap<Post, PostReviewViewModel>().ReverseMap();
            CreateMap<Tag, TagViewModel>().ReverseMap();
            CreateMap<Tag, UpdateTagViewModel>().ReverseMap();
            CreateMap<Comment, CommentViewModel>().ReverseMap();
            CreateMap<Comment, CreateCommentViewModel>().ReverseMap();
            CreateMap<Comment, CommentViewUser>().ReverseMap();

            CreateMap<AppUser, UserViewModel>().ReverseMap();
            CreateMap<AppUser, CreateUserViewModel>().ReverseMap();
            CreateMap<AppUser, UpdateUserViewModel>()
                .ForMember(des=>des.Id, opt=>opt.MapFrom(src=>src.Id))
                .ForMember(des => des.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(des => des.Email, opt => opt.MapFrom(src => src.Email))
                .ReverseMap();
            CreateMap<AppUser, LoginUserViewModel>().ReverseMap();
        }
    }
}
