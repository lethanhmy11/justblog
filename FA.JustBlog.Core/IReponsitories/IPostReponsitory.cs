﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using System;
using System.Collections.Generic;

namespace FA.JustBlog.Core.IReponsitories
{
    public interface IPostReponsitory:IGenericReponsitory<Post>
    {
        Post FindPost(int year, int month, string urlSlug);
        Post FindPost(int postId);
        IList<Post> GetPublisedPosts();
        IList<Post> GetUnpublisedPosts();
        IList<Post> GetLatestPost(int size);
        IList<Post> GetPostsByMonth(DateTime monthYear);
        int CountPostsForCategory(string category);
        IList<Post> GetPostsByCategory(string category);
        int CountPostsForTag(string tag);
        IList<Post> GetPostsByTag(string tag);
        IList<Post> GetMostViewedPost(int size);
        IList<Post> GetHighestPosts(int size);
        IList<Post> Search(string search);
    }
}
