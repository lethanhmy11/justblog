﻿using System;


namespace FA.JustBlog.ViewModels.Posts
{
    public class PostReviewViewModel
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string ShortDescription { get; set; }

        public int ViewCount { set; get; }

        public double Rate { set; get; }

        public DateTime ModifiedOn { get; set; }

        public string UrlSlug { set; get; }

    }
}
