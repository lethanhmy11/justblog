﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Infastructer
{
    public interface IGenericReponsitory<TEntity>
    {
        TEntity Find(int Id);
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(int Id);
        IList<TEntity> GetAll();

        //Task<T> GetAsync(int? id);

        //Task<List<T>> GetAllAsync();

        //Task<T> AddAsync(T entity);

        //Task AddRangeAsync(List<T> entities);

        //Task<bool> Exits(int id);

        //Task DeleteAsync(int id);

        //Task UpdateAsync(T entity);
    }
}
