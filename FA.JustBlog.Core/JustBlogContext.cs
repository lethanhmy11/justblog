﻿using FA.JustBlog.Core.BaseEntities;
using FA.JustBlog.Core.Configurations;
using FA.JustBlog.Core.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;


namespace FA.JustBlog.Core
{
    public class JustBlogContext : IdentityDbContext<AppUser>
    {
        public JustBlogContext()
        {
        }

        public JustBlogContext(DbContextOptions<JustBlogContext> options) : base(options)
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostTagMap> PostTagMaps { get; set; }
        public DbSet<Comment> Comments { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            if (!optionsBuilder.IsConfigured)
            {
                string connectionString = @"Server =DESKTOP-TIKIACV\SQLEXPRESS; Database = JustBlog; Trusted_Connection = True; MultipleActiveResultSets = true";
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            //modelBuilder.ApplyConfiguration(new RoleSeedConfiguration());
            //modelBuilder.ApplyConfiguration(new UserSeedConfiguration());
            //modelBuilder.ApplyConfiguration(new UserRoleSeedConfiguration());
      
            modelBuilder.Entity<Post>(post =>
            {
                post.HasOne<Category>(s => s.Category)
                .WithMany(g => g.Posts)
                .HasForeignKey(s => s.CategoryId);
            });

            modelBuilder.Entity<PostTagMap>(posttagmap =>
            {
                posttagmap.HasKey(sc => new { sc.PostId, sc.TagId });

                posttagmap.HasOne<Post>(sc => sc.Post)
                          .WithMany(s => s.PostTagMaps)
                          .HasForeignKey(sc => sc.PostId);


                posttagmap.HasOne<Tag>(sc => sc.Tag)
                          .WithMany(s => s.PostTagMaps)
                          .HasForeignKey(sc => sc.TagId);
            });

            modelBuilder.Entity<Comment>(comment =>
            {
                comment.HasOne<Post>(p => p.Post)
                        .WithMany(p => p.Comments)
                        .HasForeignKey(p => p.PostId);
            });

            modelBuilder.Seed();

        }

        public override int SaveChanges()
        {
            BeforSaveChanges();
            return base.SaveChanges();
        }

        private void BeforSaveChanges()
        {
            var entities = this.ChangeTracker.Entries();
            foreach (var entity in entities)
            {
                if (entity.Entity is BaseEntity baseEntity)
                {
                    var now = DateTime.Now;
                    switch (entity.State)
                    {
                        case EntityState.Modified:
                            baseEntity.ModifiedOn = now;
                            break;
                        case EntityState.Added:
                            baseEntity.CreatedOn = now;
                            baseEntity.ModifiedOn = now;
                            break;
                    }
                }
            }
        }
    }
}
