﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Tags;
using System;
using System.Collections.Generic;

namespace FA.JustBlog.Services.Tags
{
    public class TagServices : ITagServices
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public TagServices(IUnitOfWork unitOfWork,IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public UpdateTagViewModel Find(int id)
        {
            return mapper.Map<UpdateTagViewModel>(unitOfWork.TagResponsitory.Find(id));
        }
        public Tag FindDetails(int id)
        {
            return unitOfWork.TagResponsitory.Find(id);
        }

        public List<TagViewModel> GetAll()
        {
            return mapper.Map<List<TagViewModel>>(unitOfWork.TagResponsitory.GetAll());
        }

        public List<string> ListStringTagByCategory(string category)
        {
            return unitOfWork.TagResponsitory.ListStringTagByCategory(category);
        }

        public ResponseResult<UpdateTagViewModel> Update(UpdateTagViewModel tag)
        {
            try
            {
                unitOfWork.TagResponsitory.Update(mapper.Map<Tag>(tag));
                unitOfWork.SaveChange();
                return new ResponseResult<UpdateTagViewModel>();
            }
            catch(Exception ex)
            {
                return new ResponseResult<UpdateTagViewModel>(ex.Message);
            }
        }
    }
}
