﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using System.Collections.Generic;

namespace FA.JustBlog.Core.IReponsitories
{
    public interface ITagReponsitory:IGenericReponsitory<Tag>
    {
        Tag GetTagByUrlSlug(string urlSlug);
        IEnumerable<int> AddTagByString(string tags);
        string ListStringTag(int postId);
        List<string> ListStringTagByCategory(string category);
        Tag GetTagByName(string name);
    }
}
