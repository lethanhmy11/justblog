﻿using FA.JustBlog.Core.Entities;
using System.Collections.Generic;
using FA.JustBlog.Core.Infastructer;

namespace FA.JustBlog.Core.IReponsitories
{
    public interface IPostTagMapReponsitory:IGenericReponsitory<PostTagMap>
    {
        PostTagMap GetPostTagMap(int IdPost, int IdTag);
        IList<PostTagMap> GetPostTagMapByPostId(int postid);
        void RemovePostTagMap(PostTagMap postTagMap);
        void RemovePostTagMapByPostId(int PostId);
        //kiểm tra có Post nào sử dụng Tag có Id=TagId 
        bool CheckTagIsActice(int TagId);
        bool CheckTagIsActiceByName(string tagNane);
    }
}
