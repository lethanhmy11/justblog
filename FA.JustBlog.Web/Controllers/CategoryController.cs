﻿using FA.JustBlog.Services.Posts;
using FA.JustBlog.Services.Tags;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Web.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IPostServices postServices;
        private readonly ITagServices tagServices;

        public CategoryController(IPostServices postServices, ITagServices tagServices)
        {
            this.postServices = postServices;
            this.tagServices = tagServices;
        }

        public IActionResult Index(string name)
        {
            ViewBag.CategoryName = name;
            ViewBag.Tags = tagServices.ListStringTagByCategory(name);
            return View(postServices.GetPostsByCategory(name));
        }

    }
}
