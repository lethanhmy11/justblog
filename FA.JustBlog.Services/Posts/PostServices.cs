﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.ViewModels.Posts;
using FA.JustBlog.ViewModels.ResponseResults;
using System;
using System.Collections.Generic;


namespace FA.JustBlog.Services.Posts
{
    public class PostServices : IPostServices
    {
        private readonly IUnitOfWork unitOfWork;
        private readonly IMapper mapper;

        public PostServices(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this.unitOfWork = unitOfWork;
            this.mapper = mapper;
        }

        public ResponseResult<CreatePostViewModel> Create(CreatePostViewModel request)
        {
            try
            {
                var tagIds = this.unitOfWork.TagResponsitory.AddTagByString(request.Tags);               
                var postTagMaps = new List<PostTagMap>();
                foreach (var tag in tagIds)
                {
                    var postTagMap = new PostTagMap()
                    {
                        TagId = tag
                    };
                    postTagMaps.Add(postTagMap);
                }
                var post = mapper.Map<Post>(request);
                post.PostTagMaps = postTagMaps;
                this.unitOfWork.PostResponsitory.Add(post);
                this.unitOfWork.SaveChange();
                return new ResponseResult<CreatePostViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreatePostViewModel>(ex.Message);
            }
        }

        public ResponseResult<int> Delete(int id)
        {
            try
            {
                this.unitOfWork.PostResponsitory.Delete(id);
                this.unitOfWork.SaveChange();
                //xóa những tag bị loại bỏ không có post nào dùng
                var listTag = this.unitOfWork.TagResponsitory.GetAll();
                var tagIds = GetTagNameOfPost(id);
                foreach (var item in listTag)
                {
                    foreach (var tag in tagIds)
                    {
                        if (!this.unitOfWork.PostTagMapResponsitory.CheckTagIsActiceByName(tag))
                        {
                            this.unitOfWork.TagResponsitory.Delete(item.Id);
                            this.unitOfWork.SaveChange();
                        };
                    }
                }
                return new ResponseResult<int>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<int>(ex.Message);
            }
        }

        public Post Find(int id)
        {
            return this.unitOfWork.PostResponsitory.Find(id);
        }

        public List<PostReviewViewModel> GetAllPostReview()
        {
            return mapper.Map<List<PostReviewViewModel>>(this.unitOfWork.PostResponsitory.GetAll());
        }

        public List<PostViewModel> GetAll()
        {
            var list = this.unitOfWork.PostResponsitory.GetAll();
            return mapper.Map<List<PostViewModel>>(list);
        }

        public ResponseResult<CreatePostViewModel> Update(CreatePostViewModel request)
        {
            try
            {
                var tagIds = this.unitOfWork.TagResponsitory.AddTagByString(request.Tags);

                var postTagMaps = new List<PostTagMap>();
                foreach (var tag in tagIds)
                {
                    var postTagMap = new PostTagMap()
                    {
                        TagId = tag
                    };
                    postTagMaps.Add(postTagMap);
                }
                //xóa list PostTagMap cũ
                this.unitOfWork.PostTagMapResponsitory.RemovePostTagMapByPostId(request.Id);
                this.unitOfWork.SaveChange();

                Post post = this.unitOfWork.PostResponsitory.Find(request.Id);
                post.Title = request.Title;
                post.UrlSlug = request.UrlSlug;
                post.CategoryId = request.CategoryId;
                post.ShortDescription = request.ShortDescription;
                post.PostContent = request.PostContent;
                post.PostTagMaps = postTagMaps;
                post.Published = request.Published;
                post.Status = request.Status;
                this.unitOfWork.PostResponsitory.Update(post);
                this.unitOfWork.SaveChange();

                //xóa những tag bị loại bỏ không có post nào dùng
                var listTag = this.unitOfWork.TagResponsitory.GetAll();
                foreach (var item in listTag)
                {
                    foreach (var tag in tagIds)
                    {
                        if (!this.unitOfWork.PostTagMapResponsitory.CheckTagIsActice(tag))
                        {
                            this.unitOfWork.TagResponsitory.Delete(item.Id);
                            this.unitOfWork.SaveChange();
                        };
                    }
                }
                return new ResponseResult<CreatePostViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<CreatePostViewModel>(ex.Message);
            }
        }

        public List<PostReviewViewModel> GetPostsByCategory(string category)
        {
            return mapper.Map<List<PostReviewViewModel>>(unitOfWork.PostResponsitory.GetPostsByCategory(category));
        }

        public Post FindPost(int year, int month, string urlSlug)
        {
            return unitOfWork.PostResponsitory.FindPost(year, month, urlSlug);
        }

        public List<PostReviewViewModel> GetPostsByTag(string tag)
        {
            return mapper.Map<List<PostReviewViewModel>>(unitOfWork.PostResponsitory.GetPostsByTag(tag));
        }

        public List<string> GetTagNameOfPost(int id)
        {
            var stringTag=unitOfWork.TagResponsitory.ListStringTag(id);
            var tags = stringTag.Split(";");
            var listTag=new List<string>();
            foreach (var tag in tags)
            {
                listTag.Add(tag);
            }
            return listTag;
        }

        public string CategoryNameByPost(int id)
        {
            var post = unitOfWork.PostResponsitory.Find(id);
            var category = unitOfWork.CategoryResponsitory.Find(post.CategoryId);
            return category.Name;
        }
        public string StringTagOfPost(int id)
        {
            return unitOfWork.TagResponsitory.ListStringTag(id);
        }

        public List<PostViewModel> Search(string sreach)
        {
            if (String.IsNullOrEmpty(sreach))
            {
                return mapper.Map<List<PostViewModel>>(unitOfWork.PostResponsitory.GetAll());
            }
            return mapper.Map<List<PostViewModel>>(unitOfWork.PostResponsitory.Search(sreach));

        }

        public List<PostReviewViewModel> GetMostViewedPost(int size)
        {
            return mapper.Map<List<PostReviewViewModel>>(unitOfWork.PostResponsitory.GetMostViewedPost(size));
        }

        public List<PostReviewViewModel> GetHighestPosts(int size)
        {
            return mapper.Map<List<PostReviewViewModel>>(unitOfWork.PostResponsitory.GetHighestPosts(size));
        }
    }
}
