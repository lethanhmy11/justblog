﻿using FA.JustBlog.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.ViewModels.Posts
{
    public class CreatePostViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required")]
        [StringLength(255)]
        public string Title { get; set; }

        [Required(ErrorMessage = "ShortDescription is required")]
        [StringLength(1024)]
        public string ShortDescription { get; set; }

        [Required(ErrorMessage = "PostContent name is required")]
        public string PostContent { get; set; }

        [Required(ErrorMessage = "UrlSlug is required")]
        [StringLength(255)]
        public string UrlSlug { get; set; }

        public bool Published { get; set; }

        public int CategoryId { get; set; }

        [Display(Name = "Danh sách Tags (Cách nhau bởi dấu ;)")]
        [Required(ErrorMessage = "Tag is required")]
        public string Tags { set; get; }

        [Display(Name = "Status")]
        public EntityStatus Status { get; set; } = EntityStatus.Enable;

    }
}
