/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
    // config.uiColor = '#AADC6E';

    //config.filebrowserBrowseUrl = "/Assets/Admin/plugin/ckfinder/ckfinder.html";
    //config.filebrowserImageUrl = "/Assets/Admin/plugin/ckfinder/ckfinder.html?type=Images";
    //config.filebrowserFlashUrl = "/Assets/Admin/plugin/ckfinder/ckfinder.html?type=Flash";
    //config.filebrowserUploadUrl = "/Assets/Admin/plugin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files";
    //config.filebrowserImageUploadUrl = "/Assets/Admin/plugin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images";
    //config.filebrowserFlashUploadUrl = "/Assets/Admin/plugin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash";


    config.filebrowserBrowseUrl = "/wwwroot/lib/plugin/ckfinder/ckfinder.html";
    config.filebrowserImageUrl = "/wwwroot/lib/plugin/ckfinder/ckfinder.html?type=Images";
    config.filebrowserFlashUrl = "/wwwroot/lib/plugin/ckfinder/ckfinder.html?type=Flash";
    config.filebrowserUploadUrl = "/wwwroot/lib/plugin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Files";
    config.filebrowserImageUploadUrl = "/wwwroot/lib/plugin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Images";
    config.filebrowserFlashUploadUrl = "/wwwroot/lib/plugin/ckfinder/core/connector/aspx/connector.aspx?command=QuickUpload&type=Flash";

    


    config.extraPlugins = 'youtube';
    config.youtube_responsive = true;
};
