﻿using FA.JustBlog.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.ViewModels.Categories
{
    public class CreateCategoryViewModel
    {
        public int Id { get; set; }

        [Display(Name="Category Name")]
        [Required(ErrorMessage = "Category Name is required")]
        [StringLength(255)]
        public string Name { get; set; }

        [Display(Name = "UrlSlug")]
        [Required(ErrorMessage = "UrlSlug is required")]
        [StringLength(255)]
        public string UrlSLug { get; set; }

        [Display(Name = "Description")]
        [Required(ErrorMessage = "Description is required")]
        [StringLength(1024)]
        public string Description { get; set; }

        [Display(Name = "Status")]
        public EntityStatus Status { get; set; }=EntityStatus.Enable;


    }
}
