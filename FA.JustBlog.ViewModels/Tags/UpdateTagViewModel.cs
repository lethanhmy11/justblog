﻿using System.ComponentModel.DataAnnotations;


namespace FA.JustBlog.ViewModels.Tags
{
    public class UpdateTagViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage ="Tag Name is required")]
        [StringLength(255)]
        public string Name { get; set; }

        [Required(ErrorMessage = "UrlSLug is required")]
        [StringLength(255)]
        public string UrlSLug { get; set; }

        [Required(ErrorMessage = "Description is required")]
        [StringLength(1024)]
        public string Description { get; set; }
    }
}
