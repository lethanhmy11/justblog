﻿using FA.JustBlog.Services.Tags;
using FA.JustBlog.ViewModels.Tags;
using Microsoft.AspNetCore.Mvc;

namespace FA.JustBlog.Web.Areas.Admin.Controllers
{
    
    public class TagController : BaseController
    {
        private readonly ITagServices tagServices;

        public TagController(ITagServices tagServices)
        {
            this.tagServices = tagServices;
        }

        public IActionResult Index()
        {
            return View(tagServices.GetAll());
        }

        public IActionResult Details(int id)
        {
            return View(tagServices.FindDetails(id));
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            return View(tagServices.Find(id));
        }

        [HttpPost]
        public IActionResult Edit(UpdateTagViewModel tag)
        {
            if (ModelState.IsValid)
            {
                tagServices.Update(tag);
                return RedirectToAction(nameof(Index));
            }
            return View(tag);
        }
    }
}
