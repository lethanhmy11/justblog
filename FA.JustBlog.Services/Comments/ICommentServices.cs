﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.Comments;
using FA.JustBlog.ViewModels.ResponseResults;
using System.Collections.Generic;


namespace FA.JustBlog.Services.Comments
{
    public interface ICommentServices
    {
        ResponseResult<CreateCommentViewModel> Create(CreateCommentViewModel request);
        ResponseResult<CreateCommentViewModel> Update(CreateCommentViewModel request);
        ResponseResult<int> Delete(int id);
        List<CommentViewModel> GetCommentsForPost(int id);
        List<CommentViewUser> GetCommentsOfPostForUser(int id);
        Comment Find(int id);
        Comment AddComment(int PostId, string commenttext);
    }
}
