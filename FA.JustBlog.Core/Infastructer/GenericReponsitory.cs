﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Infastructer
{
    public class GenericReponsitory<TEntity> : IGenericReponsitory<TEntity> where TEntity : class
    {
        private readonly JustBlogContext context;
        public DbSet<TEntity> dbSet;

        public GenericReponsitory(JustBlogContext context)
        {
            this.context = context;
            this.dbSet = context.Set<TEntity>();
        }

        //public async Task<T> AddAsync(T entity)
        //{
        //    await context.AddAsync(entity);
        //    await context.SaveChangesAsync();
        //    return entity;
        //}

        //public async Task AddRangeAsync(List<T> entities)
        //{
        //    await context.AddRangeAsync(entities);
        //    await context.SaveChangesAsync();
        //}

        //public async Task DeleteAsync(int id)
        //{
        //    var entity = await dbSet.FindAsync(id);
        //    dbSet.Remove(entity);
        //    await context.SaveChangesAsync();
        //}

        //public async Task<bool> Exits(int id)
        //{
        //    var entity = await GetAsync(id);
        //    return entity != null;
        //}

        //public async Task<List<T>> GetAllAsync()
        //{
        //    return await dbSet.ToListAsync();
        //}

        //public async Task<T?> GetAsync(int? id)
        //{
        //    if (id == null)
        //    {
        //        return null;
        //    }
        //    return await dbSet.FindAsync(id);
        //}

        //public async Task UpdateAsync(T entity)
        //{
        //    //context.Entry(entity).State = EntityState.Modified;
        //    context.Update(entity);
        //    await context.SaveChangesAsync();
        //}


        public void Add(TEntity entity)
        {
            this.dbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            this.dbSet.Remove(entity);
        }

        public void Delete(int Id)
        {
            this.dbSet.Remove(Find(Id));
        }

        public TEntity Find(int Id)
        {
            return this.dbSet.Find(Id);
        }

        public IList<TEntity> GetAll()
        {
            return this.dbSet.ToList();
        }

        public void Update(TEntity entity)
        {
            this.dbSet.Update(entity);
        }
    }
}
