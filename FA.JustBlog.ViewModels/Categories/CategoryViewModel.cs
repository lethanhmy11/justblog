﻿using FA.JustBlog.Core.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.JustBlog.ViewModels.Categories
{
    public  class CategoryViewModel
    {
        public int Id { get; set; }

        public EntityStatus Status { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime ModifiedOn { get; set; }
       
        public string Name { set; get; }
    }
}
