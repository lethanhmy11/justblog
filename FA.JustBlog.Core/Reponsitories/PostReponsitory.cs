﻿using FA.JustBlog.Core.Entities;
using FA.JustBlog.Core.Infastructer;
using FA.JustBlog.Core.IReponsitories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FA.JustBlog.Core.Reponsitories
{
    public class PostReponsitory : GenericReponsitory<Post>, IPostReponsitory
    {
        private readonly JustBlogContext context;

        public PostReponsitory(JustBlogContext context) : base(context)
        {
            this.context = context;
        }

        public int CountPostsForCategory(string category)
        {
            var find=this.context.Categories.Where(x=>x.Name==category).FirstOrDefault();
            return this.context.Posts.Where(p => p.CategoryId == find.Id).ToList().Count();
        }

        public int CountPostsForTag(string tag)
        {
            var find = this.context.Tags.Where(x => x.Name == tag).FirstOrDefault();
            return this.context.PostTagMaps.Where(p => p.TagId == find.Id).ToList().Count();
        }

        public Post FindPost(int year, int month, string urlSlug)
        {
            return this.context.Posts.Where(p => p.ModifiedOn.Year == year && p.ModifiedOn.Month == month
            && p.UrlSlug == urlSlug).FirstOrDefault();
        }

        public Post FindPost(int postId)
        {
            return this.context.Posts.Where(p=>p.Id==postId).FirstOrDefault();
        }

        public IList<Post> GetHighestPosts(int size)
        {
            return this.context.Posts.OrderByDescending(p => p.Rate).Take(size).ToList();
        }

        public IList<Post> GetLatestPost(int size)
        {
            return this.context.Posts.OrderByDescending(p => p.ModifiedOn).Take(size).ToList();
        }

        public IList<Post> GetMostViewedPost(int size)
        {
            return this.context.Posts.OrderByDescending(p => p.ViewCount).Take(size).ToList();
        }

        public IList<Post> GetPostsByCategory(string category)
        {
            var find = this.context.Categories.Where(x => x.Name == category).FirstOrDefault();
            return this.context.Posts.Where(p => p.CategoryId == find.Id).ToList();
        }

        public IList<Post> GetPostsByMonth(DateTime monthYear)
        {
            return this.context.Posts.Where(p => p.ModifiedOn.Month == monthYear.Month &&
            p.ModifiedOn.Year == monthYear.Year).ToList();
        }

        public IList<Post> GetPostsByTag(string tag)
        {
            var liPost = from p in context.Posts
                         join c in context.PostTagMaps on p.Id equals c.PostId
                         join t in context.Tags on c.TagId equals t.Id
                         where t.Name == tag && p.Status != 0
                         select (p);
            return liPost.ToList();
        }

        public IList<Post> GetPublisedPosts()
        {
            return this.context.Posts.Where(p => p.Published == true).ToList();
        }

        public IList<Post> GetUnpublisedPosts()
        {
            return this.context.Posts.Where(p => p.Published == false).ToList();
        }

        public IList<Post> Search(string search)
        {
            
            return this.context.Posts.Where(p => p.Title.ToLower().Contains(search.ToLower())).ToList();
           
        }
    }
}
