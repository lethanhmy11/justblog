﻿using AutoMapper;
using FA.JustBlog.Core.Entities;
using FA.JustBlog.ViewModels.ResponseResults;
using FA.JustBlog.ViewModels.Users;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;


namespace FA.JustBlog.Services.Users
{
    public class UserServices : IUserServices
    {
        private readonly UserManager<AppUser> userManager;
        private readonly SignInManager<AppUser> signInManager;
        private readonly IMapper mapper;
        private readonly IPasswordHasher<AppUser> passwordHasher;
        private readonly IPasswordValidator<AppUser> passwordValid;
        private readonly IUserValidator<AppUser> userValid;

        public UserServices(UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IMapper mapper
            , IPasswordHasher<AppUser> passwordHash, IPasswordValidator<AppUser> passwordVal, IUserValidator<AppUser> userValid)
        {
            this.userManager = userManager;
            this.signInManager = signInManager;
            this.mapper = mapper;
            this.passwordHasher = passwordHash;
            this.passwordValid = passwordVal;
            this.userValid = userValid;
        }

        public async Task<ResponseResult<UserViewModel>> CreateAsync(CreateUserViewModel request)
        {
            try
            {
                
                var user = new AppUser()
                {
                    UserName = request.UserName,
                    Email = request.Email,           
                    PhoneNumber=request.PhoneNumber,
                    
                };
                var isExistEmail = userManager.Users.Where(p=>p.Email.Equals(request.Email)).FirstOrDefault();
                if(isExistEmail!=null)
                    return new ResponseResult<UserViewModel>("Email is exist");
                var result = await userManager.CreateAsync(user, request.Password);
                if (result.Succeeded)
                    return new ResponseResult<UserViewModel>();
                return new ResponseResult<UserViewModel>(result.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<UserViewModel>(ex.Message);
            }
        }

        public async Task<ResponseResult<UserViewModel>> DeleteAsync(string id)
        {
            try
            {
                var user = userManager.Users.FirstOrDefault(x => x.Id == id);             
                var result = await userManager.DeleteAsync(user);
                if (result.Succeeded)
                        return new ResponseResult<UserViewModel>();
                
                return new ResponseResult<UserViewModel>(result.Errors.Select(x => x.Description).ToArray());
            }
            catch (Exception ex)
            {
                return new ResponseResult<UserViewModel>(ex.Message);
            }
        }

        public AppUser Find(string id)
        {
            return userManager.Users.FirstOrDefault(x => x.Id == id);
        }

        public UpdateUserViewModel FindUpdateUserViewModel(string id)
        {
            return mapper.Map<UpdateUserViewModel>(Find(id));
        }

        public List<UserViewModel> GetAll()
        {
            return mapper.Map<List<UserViewModel>>(userManager.Users.ToList());
        }

        public async Task<ResponseResult<UserViewModel>> RoleAssignAsync(string email, params string[] roleNames)
        {
            try
            {
                var userExisting = await userManager.FindByEmailAsync(email);
                if (userExisting == null)
                    return new ResponseResult<UserViewModel>($"{email} khong ton tai");

                foreach (var role in roleNames)
                {
                    if (!await userManager.IsInRoleAsync(userExisting, role))
                    {
                        var result = await userManager.AddToRoleAsync(userExisting, role);
                        if (!result.Succeeded)
                            return new ResponseResult<UserViewModel>(result.Errors.Select(x => x.Description).ToArray());
                    }
                }
                return new ResponseResult<UserViewModel>();
            }
            catch (Exception ex)
            {
                return new ResponseResult<UserViewModel>(ex.Message);
            }
        }

        public async Task<ResponseResult<UserViewModel>> SignInAsync(LoginUserViewModel request)
        {
            try
            {
                var userExisting = await userManager.FindByNameAsync(request.UserName);
                if (userExisting == null)
                    return new ResponseResult<UserViewModel>($"User: {request.UserName} khong ton tai");
                var result = await signInManager.PasswordSignInAsync(userExisting, request.Password, request.RememberMe, false);
                if (result.Succeeded)
                    return new ResponseResult<UserViewModel>();
                if (result.IsLockedOut)
                    return new ResponseResult<UserViewModel>("Tai khoan cua ban da bi khoa");
                return new ResponseResult<UserViewModel>("Dang nhap khong thanh cong");
            }
            catch (Exception ex)
            {
                return new ResponseResult<UserViewModel>(ex.Message);
            }
        }

        public async Task<ResponseResult<UserViewModel>> UpdateAsync(UpdateUserViewModel request)
        {
            try
            {
                //if (request.OldPassword == null && request.Password != null)
                //{
                //    return new ResponseResult<UserViewModel>("Old Password is required");
                //}
                //if (request.OldPassword != null && request.Password == null)
                //{
                //    return new ResponseResult<UserViewModel>("New Password is required");
                //}
                //if (request.OldPassword != null && request.Password != null && !request.OldPassword.Equals(request.Password))
                //{
                //    return new ResponseResult<UserViewModel>("Password is incorrect");
                //}
                //var result = await userManager.UpdateAsync(mapper.Map<AppUser>(request));
                //if (result.Succeeded)
                //    return new ResponseResult<UserViewModel>();
                AppUser user = await userManager.FindByIdAsync(request.Id);
                user.UserName = request.UserName;
                user.PhoneNumber = request.PhoneNumber;
                IdentityResult validEmail = null;
                if (!string.IsNullOrEmpty(request.Email))
                {
                    validEmail = await userValid.ValidateAsync(userManager, user);
                    if (validEmail.Succeeded)
                        user.Email = request.Email;
                    else
                        return new ResponseResult<UserViewModel>("Email is in valid");
                }
                IdentityResult result = await userManager.UpdateAsync(user);
                if (result.Succeeded)
                    return new ResponseResult<UserViewModel>();
                else
                    return new ResponseResult<UserViewModel>(result.Errors.Select(x => x.Description).ToArray());
                
                //AppUser user = await userManager.FindByIdAsync(request.Id);
                //if (user != null)
                //{
                //    IdentityResult validEmail = null;
                //    if (!string.IsNullOrEmpty(request.Email))
                //    {
                //        validEmail = await userValid.ValidateAsync(userManager, user);
                //        if (validEmail.Succeeded)
                //            user.Email = request.Email;
                //        else
                //            return new ResponseResult<UserViewModel>("Email is in valid");
                //    }
                //    IdentityResult validPass = null;
                //    if (!string.IsNullOrEmpty(request.Password))
                //    {
                //        validPass = await passwordValid.ValidateAsync(userManager, user, request.Password);
                //        if (validPass.Succeeded)
                //            user.PasswordHash = passwordHasher.HashPassword(user, request.Password);
                //        else
                //            return new ResponseResult<UserViewModel>("Passwork is in valid");
                //    }

                //    if (validEmail != null && validPass != null && validEmail.Succeeded && validPass.Succeeded)
                //    {
                //        IdentityResult result = await userManager.UpdateAsync(user);
                //        if (result.Succeeded)
                //            return new ResponseResult<UserViewModel>();
                //        else
                //            return new ResponseResult<UserViewModel>(result.Errors.Select(x => x.Description).ToArray());
                //    }
                //    else
                //    {
                //        return new ResponseResult<UserViewModel>("Have an error");
                //    }
                //}
                //return new ResponseResult<UserViewModel>("Have an error");
            }
            catch (Exception ex)
            {
                return new ResponseResult<UserViewModel>(ex.Message);
            }
        }

       
    }
}
