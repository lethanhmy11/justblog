﻿using FA.JustBlog.Core.IReponsitories;
using FA.JustBlog.Core.Reponsitories;
using System.Threading.Tasks;

namespace FA.JustBlog.Core.Infastructer
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly JustBlogContext context;
        private IPostReponsitory postResponsitory;
        private ITagReponsitory tagResponsitory;
        private ICategoryReponsitory categoryResponsitory;
        private IPostTagMapReponsitory postTagMapResponsitory;
        private ICommentReponsitory commentResponsitory;

        public UnitOfWork(JustBlogContext context)
        {
            this.context = context;
        }

        public ICategoryReponsitory CategoryResponsitory
        {
            get
            {
                if(this.categoryResponsitory == null)
                {
                    this.categoryResponsitory = new CategoryReponsitory(this.context);
                }
                return this.categoryResponsitory;
            }
        }

        public IPostReponsitory PostResponsitory
        {
            get
            {
                if (this.postResponsitory==null)
                {
                    this.postResponsitory = new PostReponsitory(this.context);
                }
                return this.postResponsitory;
            }
        }

        public ITagReponsitory TagResponsitory
        {
            get
            {
                if (this.tagResponsitory == null)
                {
                    this.tagResponsitory = new TagReponsitory(this.context);
                }
                return this.tagResponsitory;
            }
        }

        public IPostTagMapReponsitory PostTagMapResponsitory
        {
            get
            {
                if (this.postTagMapResponsitory== null)
                {
                    this.postTagMapResponsitory = new PostTagMapReponsitory(this.context);
                }
                return this.postTagMapResponsitory;
            }
        }

        public ICommentReponsitory CommentResponsitory
        {
            get
            {
                if (this.commentResponsitory == null)
                {
                    this.commentResponsitory = new CommentReponsitory(this.context);
                }
                return this.commentResponsitory;
            }
        }

        public void Dispose()
        {
            this.context.Dispose();
        }

        public int SaveChange()
        {
            return this.context.SaveChanges();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await this.context.SaveChangesAsync();
        }
    }
}
