﻿using System.ComponentModel.DataAnnotations;


namespace FA.JustBlog.ViewModels.Accounts
{
    public class ChangePassword
    {
        [Required]
        public string CurrentPassword { get; set; }

        [Required]
        public string NewPassword { get; set; }

        [Required]
        public string ConfirmPassword { get; set; }

        public string Email { get; set; }

    }
}
