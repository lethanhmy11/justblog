﻿namespace FA.JustBlog.ViewModels.ResponseResults
{
    public class ResponseResult<T>
    {
        public ResponseResult()
        {
            this.IsSuccessed = true;
        }
        public ResponseResult(T[] data)
        {
            this.IsSuccessed = true;
            this.Data = data;
        }
        public ResponseResult(params string[] errrorMessage)
        {
            this.IsSuccessed = false;
            this.ErrrorMessages = errrorMessage;
        }
        public bool IsSuccessed { get; set; }
        public T[] Data { get; set; }
        public string[] ErrrorMessages { get; set; }


        //public ResponseResult()
        //{
        //    IsSuccessed = true;
        //}

        //public ResponseResult(string errorMessage)
        //{
        //    IsSuccessed = false;
        //    ErrorMessage = errorMessage;
        //}

        //public bool IsSuccessed { get; set; }

        //public string ErrorMessage { get; set; }
    }
}
